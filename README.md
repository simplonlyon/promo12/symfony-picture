# Symfony ORM
Projet symfony avec le web-skeleton et toute la puissance du framework

## How To use
1. Cloner le projet
2. Faire un `composer install`
3. Créer le .env.local avec la DATABASE_URL dedans (en se basant sur celle qu'il y a dans le .env)
4. (optionnel) Faire un `bin/console doctrine:database:create` pour créer la bdd si elle n'existe pas
5. Faire un `bin/console doctrine:migrations:migrate` pour exécuter les dernière migrations et mettre à jour la base de données
6. Faire un `bin/console doctrine:fixtures:load` pour charger les données de test

Si ça ne marche pas, faire un `bin/console doctrine:database:drop --force` et reprendre à l'étape 4

## How To Test
1. Mettre les bonnes informations de connexion dans le .env.test si c'est pas déjà le cas
2. `bin/console do:da:cr --env=test` pour créer la bdd de test
3. `bin/console do:mi:mi --env=test` pour migrer vers la bdd de test
4. `bin/phpunit` pour lancer les tests

Pareil, si ça marche pas, on drop avant de faire le 2 `bin/console do:da:dr --force --env=test`