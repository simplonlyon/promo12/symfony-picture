<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\User;
use App\Form\PictureType;
use App\Repository\PictureRepository;
use App\Service\FileUploader;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/user/picture/add", name="picture_add")
     */
    public function index(Request $request, ObjectManager $manager, FileUploader $uploader)
    {
        $picture = new Picture();
        $form = $this->createForm(PictureType::class, $picture);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $imgPath = $uploader->upload($picture->getUploadImage());
            $picture->setImage($imgPath);

            $picture->setDate(new \DateTime())
            ->setOwner($this->getUser());
            $manager->persist($picture);
            $manager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('account/add_picture.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * On récupère via le ParamConverter le user que l'on souhaite follow
     * @Route("/user/subscribe/{id}", name="user_subscribe")
     */
    public function subscribe(ObjectManager $manager, User $toFollow) {        
       /*On récupère le user actuellement connecté et ensuite on fait
       appel à la méthode addSubscription de celui ci (cette méthode est
       possible car elle existe dans l'entité App\Entity\User) en 
       lui donnant en argument le user qu'on a récupéré via la route */
        $this->getUser()->addSubscription($toFollow);
        //On flush pour appliquer la modification à la base de données
        $manager->flush();
        //On redirige vers une autre page
        return $this->redirectToRoute('user_feed');
    }
    /**
     * @Route("/user/feed", name="user_feed")
     */
    public function feed(PictureRepository $repo) {

        return $this->render("account/feed.html.twig",[
            //On récupère via la méthode custom toutes les pictures
            //postées par les personnes que follow le user actuellement connecté
            'pictures' => $repo->findBySubscriptions($this->getUser())
        ]);
    }
}
