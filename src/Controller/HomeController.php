<?php

namespace App\Controller;

use App\Repository\PictureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PictureRepository $repo)
    {
        return $this->render('home/index.html.twig', [
            'pictures' => $repo->findAll(),
        ]);
    }
}
