<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i=0; $i < 5; $i++) { 
            $user = new User();
            $user->setAlias($faker->userName)
            ->setEmail($faker->email)
            ->setPassword($this->encoder->encodePassword($user, '1234'))
            ->setRoles(['ROLE_USER']);

            if($i === 0) {
                $user->setEmail('mail@mail.com');
            }

            for ($y=0; $y < 3; $y++) { 
                $picture = new Picture();
                $picture->setOwner($user)
                ->setDate($faker->dateTime())
                ->setImage($faker->imageUrl(640, 480, 'cats'))
                ->setAlt('cute cat...')
                ->setDescription($faker->text());
                $manager->persist($picture);
            }

            $manager->persist($user);
        }
        


        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
