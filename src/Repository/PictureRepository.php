<?php

namespace App\Repository;

use App\Entity\Picture;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Picture|null find($id, $lockMode = null, $lockVersion = null)
 * @method Picture|null findOneBy(array $criteria, array $orderBy = null)
 * @method Picture[]    findAll()
 * @method Picture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Picture::class);
    }

    /**
     * On ne rajoutera de nouvelles méthodes dans un repository que
     * lorsqu'on aura besoin de requêter une ou des entités de manière
     * très spécifique.
     * Ici, on veut récupérer la liste des pictures postées par
     * les Users qu'un autre User spécifique follow
     * @return Picture[] Returns an array of Picture objects
     */
    public function findBySubscriptions(User $user)
    {
        //On commence par créer un alias pour les picture
        return $this->createQueryBuilder('p')
        //On fait une jointure avec la propriété owner du picture (qui est un User)
            ->leftJoin('p.owner', 'o')
            //on vérifie si le $user en paramètre de la méthode fait partie des
            //followers du owner de chaque picture
            ->andWhere(':user MEMBER OF o.followers')
            //(on assigne le $user au placeholder du where)
            ->setParameter('user', $user)
            //On ordonne les pictures récupérées par leur propriété date du plus récent au plus ancien
            ->orderBy('p.date', 'DESC')
            //On limite le nombre de résultat renvoyé à 30
            ->setMaxResults(30)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?Picture
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
