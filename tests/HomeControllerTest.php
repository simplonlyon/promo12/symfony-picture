<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class HomeControllerTest extends WebTestCase
{

    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();   
    }

    public function testPicturesDisplay()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertCount(15, $crawler->filter('.card'));
    }
}
