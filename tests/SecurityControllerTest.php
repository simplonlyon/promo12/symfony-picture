<?php

namespace App\Tests;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class SecurityControllerTest extends WebTestCase
{
    /**
     * Méthode qui se déclenchera avant le début de chaque test de
     * la classe actuelle. Elle est utile pour mettre en place 
     * l'environnement de test, ici on l'utilise pour remettre à 
     * zéro la base de données en relancant la fixture
     */
    public function setUp() {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();   
    }

    public function testRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        //On récupère le formulaire en se basant sur le texte du button submit
        $form = $crawler->selectButton('Register')->form();
        //On rempli les différents champs du formulaire
        $form['user[email]'] = 'test@test.com';
        $form['user[password][first]'] = '1234';
        $form['user[password][second]'] = '1234';
        $form['user[alias]'] = 'test';
        //On submit le formulaire
        $client->submit($form);
        //On vérifie si une redirection est bien déclenchée vers la 
        //page d'accueil, indiquant le succès du formulaire
        $this->assertResponseRedirects('/');
        //On récupère le UserRepository "manuellement"
        $repo = static::$container->get('App\Repository\UserRepository');
        //On vérifie qu'un nouvel user a bien été ajouté dans la bdd
        $this->assertCount(6, $repo->findAll());

    }
    /**
     * Ne pas hésiter à faire des tests pour les cas où ce n'est pas
     * sensé marcher
     */
    public function testRegisterNotValid()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        //On récupère le formulaire en se basant sur le texte du button submit
        $form = $crawler->selectButton('Register')->form();
        //On rempli les différents champs du formulaire avec des valeurs incorrectes
        $form['user[email]'] = 'test';
        $form['user[password][first]'] = '1234';
        $form['user[password][second]'] = '34';
        $form['user[alias]'] = 'test';

        $client->submit($form);
        //On vérifie si on a bien les messages d'erreur pour les deux champs incorrects
        $this->assertSelectorTextContains('[for="user_password_first"]', 'This value is not valid.');
        $this->assertSelectorTextContains('[for="user_email"]', 'This value is not a valid email address.');
        
        $repo = static::$container->get('App\Repository\UserRepository');
        //On vérifie que rien a été ajouté en base de données
        $this->assertCount(5, $repo->findAll());

    }
}
